#include "../src/Alarm.hpp"
#include "../src/StateMachine.hpp"

#include <variant>
#include <chrono>

#include "gtest/gtest.h"



TEST(alarmtest, alarmEvent) {
    Alarm::EventManager eventManager;
    EXPECT_EQ(std::nullopt, eventManager.handleInput('t'));
    EXPECT_EQ(3, eventManager.handleInput('h')->index());
    EXPECT_EQ(std::nullopt, eventManager.handleInput('m'));
    EXPECT_EQ(std::nullopt, eventManager.handleInput('l'));
    EXPECT_EQ(2, eventManager.handleInput('h')->index());
    EXPECT_EQ(1, eventManager.handleInput('m')->index());
    EXPECT_EQ(0, eventManager.handleInput('l')->index());
    EXPECT_EQ(1, eventManager.handleInput('l')->index());
    EXPECT_EQ(2, eventManager.handleInput('m')->index());
    EXPECT_EQ(3, eventManager.handleInput('h')->index());
    EXPECT_EQ(std::nullopt, eventManager.handleInput('m'));
    EXPECT_EQ(std::nullopt, eventManager.handleInput('l'));
    EXPECT_EQ(0, eventManager.handleInput('h')->index());
    EXPECT_EQ(2, eventManager.handleInput('m')->index());
    EXPECT_EQ(0, eventManager.handleInput('m')->index());
    eventManager.handleInput('l');
    EXPECT_EQ(3, eventManager.handleInput('h')->index());

}

TEST(alarmtest, alarmHigh) {
    std::chrono::time_point<std::chrono::steady_clock> now;
    Alarm::StateHigh stateHigh{now};
    EXPECT_FALSE(stateHigh.isBipOn());
    stateHigh.execAlarm(now);
    EXPECT_TRUE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(249);
    stateHigh.execAlarm(now);
    EXPECT_TRUE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(1);
    stateHigh.execAlarm(now);
    EXPECT_FALSE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_TRUE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_FALSE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_TRUE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_FALSE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_TRUE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_FALSE(stateHigh.isBipOn());
    now += std::chrono::milliseconds(250);
    stateHigh.execAlarm(now);
    EXPECT_TRUE(stateHigh.isBipOn());
    for(int i = 0; i < 8; ++i){
        now += std::chrono::milliseconds(250);
        stateHigh.execAlarm(now);
        EXPECT_FALSE(stateHigh.isBipOn());
    }
    for(int cpt = 0; cpt < 10; ++cpt){
        for(int i = 0; i < 5 * 2; ++i){
            now += std::chrono::milliseconds(250);
            stateHigh.execAlarm(now);
            if(i % 2 == 0)
                EXPECT_TRUE(stateHigh.isBipOn());
            else
               EXPECT_FALSE(stateHigh.isBipOn()); 
        }
        for(int i = 0; i < 7; ++i){
            now += std::chrono::milliseconds(250);
            stateHigh.execAlarm(now);
            EXPECT_FALSE(stateHigh.isBipOn());
        }
    }
}

TEST(alarmtest, alarmMedium) {
    std::chrono::time_point<std::chrono::steady_clock> now;
    Alarm::StateMedium stateMedium{now};
    EXPECT_FALSE(stateMedium.isBipOn());
    stateMedium.execAlarm(now);
    EXPECT_TRUE(stateMedium.isBipOn());
    now += std::chrono::milliseconds(249);
    stateMedium.execAlarm(now);
    EXPECT_TRUE(stateMedium.isBipOn());
    now += std::chrono::milliseconds(1);
    stateMedium.execAlarm(now);
    EXPECT_FALSE(stateMedium.isBipOn());
    now += std::chrono::milliseconds(250);
    stateMedium.execAlarm(now);
    EXPECT_FALSE(stateMedium.isBipOn());
    now += std::chrono::milliseconds(250);
    stateMedium.execAlarm(now);
    EXPECT_FALSE(stateMedium.isBipOn());
    for(int i = 0; i < 10; ++i) {
        now += std::chrono::milliseconds(250);
        stateMedium.execAlarm(now);
        EXPECT_TRUE(stateMedium.isBipOn());
        now += std::chrono::milliseconds(250);
        stateMedium.execAlarm(now);
        EXPECT_FALSE(stateMedium.isBipOn());
        now += std::chrono::milliseconds(250);
        stateMedium.execAlarm(now);
        EXPECT_FALSE(stateMedium.isBipOn());
        now += std::chrono::milliseconds(250);
        stateMedium.execAlarm(now);
        EXPECT_FALSE(stateMedium.isBipOn());
    }
}

TEST(alarmtest, alarmLow) {
    std::chrono::time_point<std::chrono::steady_clock> now;
    Alarm::StateLow stateLow{now};
    EXPECT_FALSE(stateLow.isBipOn());
    stateLow.execAlarm(now);
    EXPECT_TRUE(stateLow.isBipOn());
    for(int i = 0; i < 3; ++i) {
        now += std::chrono::milliseconds(250);
        stateLow.execAlarm(now);
        EXPECT_TRUE(stateLow.isBipOn());
    }
    now += std::chrono::milliseconds(249);
    stateLow.execAlarm(now);
    EXPECT_TRUE(stateLow.isBipOn());
    now += std::chrono::milliseconds(1);
    stateLow.execAlarm(now);
    EXPECT_FALSE(stateLow.isBipOn());
    for(int i = 0; i < (4 * 29) - 1; ++i){
        now += std::chrono::milliseconds(250);
        stateLow.execAlarm(now);
        EXPECT_FALSE(stateLow.isBipOn());
    }
    for(int cpt = 0; cpt < 10; ++cpt) {
        for(int i = 0; i < 4; ++i) {
            now += std::chrono::milliseconds(250);
            stateLow.execAlarm(now);
            EXPECT_TRUE(stateLow.isBipOn());
        }
        for(int i = 0; i < 4 * 29; ++i){
            now += std::chrono::milliseconds(250);
            stateLow.execAlarm(now);
            EXPECT_FALSE(stateLow.isBipOn());
        }
    }
}

TEST(alarmtest, statemachine) {
    std::chrono::time_point<std::chrono::steady_clock> now;
    StateMachine<Alarm::State, Alarm::Event, Alarm::Transitions> sm{Alarm::State{Alarm::StateIdle{}}};
    sm.dispatch(Alarm::EventAlarmLow{}, now);
    EXPECT_EQ(typeid(Alarm::StateLow).name(), sm.visitCurrentState([](auto &&arg){ return typeid(arg).name(); }));
    sm.dispatch(Alarm::EventAlarmIdle{}, now);
    EXPECT_EQ(typeid(Alarm::StateIdle).name(), sm.visitCurrentState([](auto &&arg){ return typeid(arg).name(); }));
    sm.dispatch(Alarm::EventAlarmMedium{}, now);
    EXPECT_EQ(typeid(Alarm::StateMedium).name(), sm.visitCurrentState([](auto &&arg){ return typeid(arg).name(); }));
    sm.dispatch(Alarm::EventAlarmMedium{}, now);
    EXPECT_EQ(typeid(Alarm::StateMedium).name(), sm.visitCurrentState([](auto &&arg){ return typeid(arg).name(); }));
    sm.dispatch(Alarm::EventAlarmHigh{}, now);
    EXPECT_EQ(typeid(Alarm::StateHigh).name(), sm.visitCurrentState([](auto &&arg){ return typeid(arg).name(); }));
}

TEST(alarmtest, Alarm) {
    Alarm::Alarm alarm;
    alarm.Run();
    std::stringstream buffer;
    std::streambuf *sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());
    std::this_thread::sleep_for(std::chrono::milliseconds(1990));
    EXPECT_EQ("________", buffer.str());
    buffer.str("");
    buffer.clear();
    alarm.handleInput('m');
    std::this_thread::sleep_for(std::chrono::milliseconds(3990));
    EXPECT_EQ("X___X___X___X___", buffer.str());
    buffer.str("");
    buffer.clear();
    alarm.handleInput('h');
    std::this_thread::sleep_for(std::chrono::milliseconds(8400));
    EXPECT_EQ("X_X_X_X_X________X_X_X_X_X________", buffer.str());
    buffer.str("");
    buffer.clear();
    alarm.handleInput('l');
    std::this_thread::sleep_for(std::chrono::milliseconds(8400));
    EXPECT_EQ("X_X_X_X_X________X_X_X_X_X________", buffer.str());
    buffer.str("");
    buffer.clear();
    alarm.handleInput('h');
    std::this_thread::sleep_for(std::chrono::seconds(4));
    EXPECT_EQ("X___X___X___X___", buffer.str());
    std::cout.rdbuf(sbuf);
}
