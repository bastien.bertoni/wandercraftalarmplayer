#ifndef STATES_HPP
#define STATES_HPP

#include <chrono>
#include <variant>


namespace Alarm{

class StateBase {
public:
  virtual ~StateBase() = default;
  virtual void execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) {}
  bool isBipOn() const { return m_isBipOn; }
protected:
  bool m_isBipOn = false;
};

class StateIdle : public StateBase {};

class StateHigh : public StateBase {
public:
  explicit StateHigh(const std::chrono::time_point<std::chrono::steady_clock> &now);
  void execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) override;
private:
  static constexpr int nbBip = 5;
  static constexpr std::chrono::milliseconds bipDuration{250};
  static constexpr std::chrono::milliseconds bipInterval{500};
  static constexpr std::chrono::seconds waitTime{2};
  int cptBip = 0;
  std::chrono::time_point<std::chrono::steady_clock> nextBip;
};

class StateMedium : public StateBase {
public:
  explicit StateMedium(const std::chrono::time_point<std::chrono::steady_clock> &now);
  void execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) override;
private:
  static constexpr std::chrono::milliseconds bipDuration{250};
  static constexpr std::chrono::seconds bipInterval{1};
  std::chrono::time_point<std::chrono::steady_clock> nextBip;
};

class StateLow : public StateBase {
public:
  explicit StateLow(const std::chrono::time_point<std::chrono::steady_clock> &now);
  void execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) override;
private:
  static constexpr std::chrono::seconds bipDuration{1};
  static constexpr std::chrono::seconds bipInterval{30};
  std::chrono::time_point<std::chrono::steady_clock> nextBip;
};

using State = std::variant<StateIdle, StateLow, StateMedium, StateHigh>;

}

#endif