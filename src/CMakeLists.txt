set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)


find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIR})

file(GLOB SOURCES "*.cpp")

add_executable(wandercraft ${SOURCES})
target_link_libraries(
  wandercraft
  PRIVATE project_options
          project_warnings
          Threads::Threads
          ${CURSES_LIBRARIES})