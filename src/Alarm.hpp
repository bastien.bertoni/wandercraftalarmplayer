#ifndef ALARM_HPP
#define ALARM_HPP

#include <thread>
#include <atomic>
#include <chrono>

#include "StateMachine.hpp"
#include "EventManager.hpp"
#include "States.hpp"
#include "Events.hpp"
#include "Transitions.hpp"

namespace Alarm{


class Alarm{
public:
  void Run();
  void Stop();
  void handleInput(const char priority);
  ~Alarm();
private:
  void exec();
  void execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now);
  bool alarmIsOn();
  std::atomic_bool m_isRunning = false;
  EventManager m_eventManager;
  StateMachine<State, Event, Transitions> m_stateMachine{State{StateIdle{}}};
  std::thread m_thread;
  std::chrono::time_point<std::chrono::steady_clock> m_nextCharacterTime;
};

}

#endif