#include <iostream>

#include <ncurses.h>

#include "Alarm.hpp"


int main(int argc, const char **argv)
{ 
  initscr();
  raw();
  noecho();
  keypad(stdscr,true);
  Alarm::Alarm alarm;
  alarm.Run();
  char command = 0;
  do{
    command = getch();
    alarm.handleInput(command);
  }
  while(command != 'q');
  alarm.Stop();
  endwin();
}