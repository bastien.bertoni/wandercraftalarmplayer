#ifndef EVENTS_HPP
#define EVENTS_HPP

#include <variant>

namespace Alarm{


struct EventAlarmIdle{

};

struct EventAlarmLow{
};

struct EventAlarmMedium{
};

struct EventAlarmHigh{
};

using Event = std::variant<EventAlarmIdle, EventAlarmLow, EventAlarmMedium, EventAlarmHigh>;

}

#endif