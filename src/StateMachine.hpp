#ifndef STATEMACHINE_HPP
#define STATEMACHINE_HPP

#include <optional>
#include <variant>
#include <mutex>

template <typename StateVariant, typename EventVariant, typename Transitions>
class StateMachine {
public:

  explicit StateMachine(const StateVariant &state) : m_currState{state} {}

  template <typename Visitor>
  auto visitCurrentState(Visitor &&vis){
  	std::lock_guard<std::mutex> guard(m_mutex);
  	return std::visit(vis, m_currState);
  }
    
  void dispatch(const EventVariant &Event, const std::chrono::time_point<std::chrono::steady_clock> &nextCharacterTime)
  {
  	  std::lock_guard<std::mutex> guard(m_mutex);
      std::optional<StateVariant> new_state = std::visit(Transitions{nextCharacterTime}, m_currState, Event);
      if (new_state){
        m_currState = *std::move(new_state);
      }
  }

private:
  StateVariant m_currState;
  std::mutex m_mutex;
};

#endif