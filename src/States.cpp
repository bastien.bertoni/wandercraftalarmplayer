#include "States.hpp"

namespace Alarm{

StateHigh::StateHigh(const std::chrono::time_point<std::chrono::steady_clock> &now) : nextBip{now} {

}

void StateHigh::execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) {
  if(m_isBipOn){
    if(now >= (nextBip + bipDuration)){
      m_isBipOn = false;
      if(cptBip >= nbBip){
        cptBip = 0;
        nextBip += bipDuration + waitTime;
      }
      else{
        nextBip += bipInterval;
      }
    }
  }
  else if(now >= nextBip){
    m_isBipOn = true;
    ++cptBip;
  }
}


StateMedium::StateMedium(const std::chrono::time_point<std::chrono::steady_clock> &now) : nextBip{now} {

}

void StateMedium::execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) {

  if(m_isBipOn){
    if(now >= (nextBip + bipDuration)){
      m_isBipOn = false;
      nextBip += bipInterval;
    }
  }
  else if(now >= nextBip){
    m_isBipOn = true;
  }
}

StateLow::StateLow(const std::chrono::time_point<std::chrono::steady_clock> &now) : nextBip{now} {

}

void StateLow::execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now) {

  if(m_isBipOn){
    if(now >= (nextBip + bipDuration)){
      m_isBipOn = false;
      nextBip += bipInterval;
    }
  }
  else if(now >= nextBip){
    m_isBipOn = true;
  }
}

}