#include "EventManager.hpp"

namespace Alarm{

std::optional<Event> EventManager::handleInput(const char priority){
  Event alarmEvent;

  switch(priority){
    case 'l':
      alarmEvent = EventAlarmLow{};
      break;
    case 'm':
      alarmEvent = EventAlarmMedium{};
      break;
    case 'h':
      alarmEvent = EventAlarmHigh{};
      break;
    default:
      return std::nullopt;
  }

  size_t prevTopIndex = std::numeric_limits<size_t>::max();
  if(!m_alarmEventMap.empty()){
  	prevTopIndex = m_alarmEventMap.crbegin()->first;
  }

  if(!m_alarmEventMap.extract(alarmEvent.index())){
    m_alarmEventMap.emplace(alarmEvent.index(), alarmEvent);
  }

  if(!m_alarmEventMap.empty()){
    const auto topLevelEvent = m_alarmEventMap.crbegin();
    return  topLevelEvent->first != prevTopIndex ? std::optional<Event>{topLevelEvent->second} : std::nullopt;
  }

  return EventAlarmIdle{};
}

}