#include "Alarm.hpp"

#include <iostream>
#include <limits>

namespace Alarm {

Alarm::~Alarm(){
  Stop();
}

void Alarm::Run(){
  if(m_isRunning)
    return;

  m_isRunning = true;
  m_thread = std::thread(&Alarm::exec, this);
  
}

void Alarm::Stop(){
  m_isRunning = false;
  if(m_thread.joinable())
  	m_thread.join();

}

void Alarm::handleInput(const char priority){
  const auto event = m_eventManager.handleInput(priority);
  if(event){
    m_stateMachine.dispatch(event.value(), m_nextCharacterTime);
  }
}

namespace{
  using namespace std::chrono_literals;
  constexpr auto refreshPeriod = 1ms;
  constexpr auto characterDuration = 250ms;
}

void Alarm::exec(){
  m_nextCharacterTime = std::chrono::steady_clock::now();

  while(m_isRunning){
    const std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    execAlarm(now);
    
    if(now >= m_nextCharacterTime){      
      std::cout << (alarmIsOn() ? 'X' : '_');
      std::cout.flush();
      m_nextCharacterTime += characterDuration;
    }
    std::this_thread::sleep_for(refreshPeriod);
  }
}

void Alarm::execAlarm(const std::chrono::time_point<std::chrono::steady_clock> &now){
  m_stateMachine.visitCurrentState([&](auto &&arg){arg.execAlarm(now);});
}

bool Alarm::alarmIsOn(){
  return m_stateMachine.visitCurrentState([](auto &&arg){ return arg.isBipOn();});
}

}