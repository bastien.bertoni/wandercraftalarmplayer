#ifndef EVENTMANAGER_HPP
#define EVENTMANAGER_HPP

#include <optional>
#include <map>

#include "Events.hpp"

namespace Alarm{

class EventManager{
public:
  std::optional<Event> handleInput(const char priority);
private:
  std::map<size_t, Event> m_alarmEventMap;
};

}

#endif