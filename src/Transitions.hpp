#ifndef TRANSITIONS_HPP
#define TRANSITIONS_HPP

#include <optional>

#include "States.hpp"
#include "Events.hpp"

namespace Alarm {

class Transitions {
public:
  explicit Transitions(const std::chrono::time_point<std::chrono::steady_clock> &nextCharacterTime) : m_nextCharacterTime{nextCharacterTime} {
    
  }

  std::optional<State> operator()(StateIdle &, const EventAlarmLow &) const {
    return StateLow{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateIdle &, const EventAlarmMedium &) const {
    return StateMedium{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateIdle &, const EventAlarmHigh &) const {
    return StateHigh{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateLow &, const EventAlarmIdle &) const {
    return StateIdle{};
  }
  std::optional<State> operator()(StateLow &, const EventAlarmMedium &) const {
    return StateMedium{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateLow &, const EventAlarmHigh &) const {
    return StateHigh{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateMedium &, const EventAlarmIdle &) const {
    return StateIdle{};
  }
  std::optional<State> operator()(StateMedium &, const EventAlarmLow &) const {
    return StateLow{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateMedium &, const EventAlarmHigh &) const {
    return StateHigh{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateHigh &, const EventAlarmIdle &) const {
    return StateIdle{};
  }
  std::optional<State> operator()(StateHigh &, const EventAlarmLow &) const {
    return StateLow{m_nextCharacterTime};
  }
  std::optional<State> operator()(StateHigh &, const EventAlarmMedium &) const {
    return StateMedium{m_nextCharacterTime};
  }
  template <typename State_t, typename Event_t>
  std::optional<State> operator()(State_t &, const Event_t &) const {
      return std::nullopt;
  }
private:
  const std::chrono::time_point<std::chrono::steady_clock> m_nextCharacterTime;
};

}

#endif